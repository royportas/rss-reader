import schedule from "node-schedule";
import pino from "pino";

const logger = pino();

schedule.scheduleJob("* * * * *", () => {
  logger.info("exampleJob invoked");
});
