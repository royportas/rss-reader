import express from "express";
import helmet from "helmet";
import pino from "pino";
import pinoHttp from "pino-http";
import "./scheduler";

const app = express();
const PORT = 4000;

app.use(helmet());

const logger = pino();

const httpLogger = pinoHttp();
app.use(httpLogger);

app.get("/", (req, res) => {
  req.log.info("Hello world from pino");
  res.send("Hello world");
});

app.listen(PORT, () => logger.info(`RSS Reader listening on http://${PORT}`));
