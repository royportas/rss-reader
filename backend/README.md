# RSS Reader Backend

This is an attempt to make a "smarter" RSS application,
"smarter" in the sense that it tries to do the following:

- Present an ordered, prioritized list of items, in the order that is most relevant to the user (based on an algorithm)

## Testing and CI/CD

The goal of this project is to embrace CI/CD.
In my mind, this encompasses the following:

- Having good unit test coverage
- Being able to do some level of integration testing
- Being able to build in a CI environment (GitLab)
- Being able to do continuous delivery to a local server

## RSS Item Heuristics

This enables making better suggestions for RSS items, some ideas for heuristics include:

- How much it matches a corpus of important search terms

The heuristics are roughly based on search engine logic

## Notifications

There are multiple levels of notifications to the user, in order of priority:

- High Priority: Delivered as push notifications over PushBullet
- Medium Priority: Aggregated together in a newsletter format delivered every day
- Low Priority: Don't notify the user, just display it in the UI

When the system identifies an RSS item that it determines as "important" to the user, it should notify them via a push notification.

## Viewing RSS Items

The interface is designed to handle large amounts of RSS items, the user interface should be designed
to allow the user to filter through and find the items they are interested in.

Some ideas for creating these interfaces include:

- Machine Learning visualizations, including t-SNE (for example, https://cs.stanford.edu/people/karpathy/tsnejs/), do it in the backend, not the frontend
